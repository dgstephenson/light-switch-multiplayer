const express = require('express')
const http = require('http')
const socketio = require('socket.io')
const path = require('path')
const app = express()
const server = http.Server(app)
const io = socketio(server)
const PORT = process.env.PORT || 5000;

var ids = []
var clients = {}
var players = {}
var lightOn = false

app.use(express.static(path.join(__dirname,'public')))

app.get('/', (request,response) => 
    response.sendFile(path.join(__dirname,'public','client.html'))
)

io.on('connection', socket => {
  console.log("socket.id =",socket.id)
  socket.emit("setup",{lightOn,players})
  socket.on('login', msg => {
    clients[socket.id] = msg
    socket.emit('loginComplete',{lightOn,players})
    updatePlayers()
  })
  socket.on('updateServer', msg => {
    clients[socket.id] = msg
    updatePlayers()
  })
  socket.on('setLight', msg => {
    console.log('setLight '+msg)
    lightOn = msg
    updatePlayers()
  })
})

updatePlayers = () => {
  ids = Object.keys(io.sockets.connected)
  players = ids.map(id => clients[id])
  ids.map(id => {
    io.sockets.connected[id].emit("updateClient",{lightOn,players})
  })
}

server.listen(PORT, () => {
  const port = server.address().port
  console.log(`listening on port: ${port}`)
})
