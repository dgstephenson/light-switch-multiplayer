var loginDiv = document.getElementById("loginDiv")
var locationDiv = document.getElementById("locationDiv")
var socket = io()

var myPlayer = {
  name: '',
  location: '',
}

var players = {}
var lightOn = false
var loginComplete = false

login = (event,name) => {
  if(event.keyCode==13 & name!='') {
    console.log('login '+name)
    myPlayer.name = name
    myPlayer.location = 'Room'
    socket.emit('login',myPlayer)
  }
}

showLocation = () => {
  if(myPlayer.location=='Room') {
    description = 'You are in a room.<br>'
    description = 'There is a switch on the wall.<br>' 
    if(lightOn) description += 'The switch is on.<br>' 
    else description += 'The switch is off.<br>'
    description += getOtherPlayerText()
    description += 'There is a door leading north.'
    locationDiv.innerHTML = getDescriptionHTML(description)
    locationDiv.innerHTML += getOptionHTML('Go North')
    locationDiv.innerHTML += getOptionHTML('Flip Switch')
  }
  if(myPlayer.location=='Hall') {
    description = 'You are in a hallway.<br>' 
    if(lightOn) description += 'There is a bright light above you.<br>' 
    else description += 'It is too dark to see anything.<br>'
    description += getOtherPlayerText()
    description += 'There is a door leading south.'
    locationDiv.innerHTML = getDescriptionHTML(description)
    locationDiv.innerHTML += getOptionHTML('Go South')
  }
}

getDescriptionHTML = (text) => "<div>"+text+"</div>"
getOptionHTML = (text) => {
  html = '<div id="'+text+'"' 
  html += 'style="color:#0000CC" class="disable-select" onclick="selectOption(this.id)">'
  html += text+'</div>'
  return html
}

selectOption = (option) => {
  console.log(option)
  if(myPlayer.location=='Room') {
    if(option=='Flip Switch') {
      socket.emit('setLight',!lightOn)
    }
    if(option=='Go North') {
      myPlayer.location = 'Hall'
      socket.emit('updateServer',myPlayer)
    }
  }
  if(myPlayer.location=='Hall') {
    if(option=='Go South') {
      myPlayer.location = 'Room'
      socket.emit('updateServer',myPlayer)
    }
  } 
}

getOtherPlayerText = () => {
  if(players.filter && myPlayer) {
    otherplayers = players.filter(player => player && player.location==myPlayer.location && player.name!=myPlayer.name)
    names = otherplayers.map(player => player.name)
    if(names.length==0) return ''
    if(names.length==1) return names[0]+' is here.<br>'
    if(names.length==2) return names[0]+' and '+names[1]+' are here.<br>'
    if(names.length>=3) {
      names[names.length-1] = 'and '+names[names.length-1]
      return names.join(', ')+' are here.<br>'
    }
  }
}

socket.on('connect', () => {

  console.log('socket.id =',socket.id)

  socket.on('setup',msg => {
    console.log('setup')
    console.log(msg)
    lightOn = msg.lightOn
    players = msg.players
    if(!loginComplete) {
      loginDiv.style.display = 'block'
      locationDiv.style.display = 'none'
    }
  })

  socket.on('loginComplete', msg => {
    console.log('loginComplete')
    lightOn = msg.lightOn
    players = msg.players
    loginDiv.style.display = 'none'
    locationDiv.style.display = 'block'
    loginComplete = true
    showLocation()
  })

  socket.on('updateClient', msg => {
    lightOn = msg.lightOn
    players = msg.players
    if(loginComplete) showLocation()
  })

})